#!/bin/bash

config=/etc/suricata/suricata.yaml

load() {
    
    if grep -q "local.rules" $config; then
        :
    else
        sudo sed -i 's/- suricata.rules/- local.rules/g' $config
    fi
    
    ! test -f local.rules && {
        echo 'alert ssh any any -> any any (msg: "SSH connection found"; flow:to_server, not_established; sid:1000001; rev:1;)
alert icmp any any -> any any (msg: "ICMP Packet found"; sid:1000002; rev:1;)
        ' > local.rules
    }
    sudo mkdir -p /var/lib/suricata/rules
    sudo cp local.rules /var/lib/suricata/rules/local.rules
    sudo kill -usr2 $(pidof suricata)
    sudo suricata -T -c /etc/suricata/suricata.yaml -v
}

install() {

    command -v suricata >/dev/null 2>&1 || {
        sudo apt update
        sudo apt install -y software-properties-common
        sudo add-apt-repository ppa:oisf/suricata-stable -y
        sudo apt -y install suricata
    }
    
    ##### CONFIGURE
    
    sudo sed -i 's/interface: eth0/interface: enp0s3/g' $config
    sudo sed -i 's/community-id: false/community-id: true/g' $config
    
    command -v ip >/dev/null 2>&1 || sudo apt install -y iproute2 >/dev/null 2>&1
    command -v jp >/dev/null 2>&1 || sudo apt install -y jp
    echo "search for af-packet:"
    # TODO if eth0 all ok
    ip -p -j route show default | jp [].dev
    
    # Live Rule Reloading
    if grep -q "rule-reload" $config; then
        :
    else
        echo "
detect-engine:
        - rule-reload: true" | sudo tee -a $config >/dev/null
    fi
    
    
    sudo systemctl restart suricata
    
    ## TODO test file exists
    #sudo suricata-update
    
    ## TODO
    # App-Layer protocol rdp enable status not set, so enabling by default. This behavior will change in Suricata 7, so please update your config. See ticket #4744 for more details.
    
    
}

log() {
    
    if ! [ -n "$1" ]; then
        tail /var/log/suricata/fast.log
    else
        if [ $1 == "reset" ]; then
            echo -n "" | sudo tee /var/log/suricata/fast.log
        else 
            re='^[0-9]+$'
            if ! [[ $1 =~ $re ]] ; then
                echo "error: Not a number" >&2; exit 1
            else
                tail -n $1 /var/log/suricata/fast.log
            fi
        fi
    fi
}


case $1 in
    install)
        install
    ;;
    log)
        log $2
    ;;
    load)
        load
    ;;
    *)
        echo "$0 install | load | log"
    ;;
esac
