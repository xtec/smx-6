#!/bin/bash

AWS_PATH=$HOME/.aws

if [ ! -f /usr/local/bin/aws ]; then
    curl "https://awscli.amazonaws.com/awscli-exe-linux-x86_64.zip" -o "awscliv2.zip"
    sudo apt update
    sudo apt install -y unzip
    unzip -q awscliv2.zip
    sudo ./aws/install
    rm awscliv2.zip
    rm -rf aws
    /usr/local/bin/aws --version
fi

# configure

echo "Pots trobar les claus d'accés a https://console.filebase.com/keys"
read -p 'KEY: ' key
read -p 'SECRET: ' secret

mkdir -p $AWS_PATH

echo "[default]
aws_access_key_id =  $key 
aws_secret_access_key = $secret
" >$AWS_PATH/credentials

    echo "[default]
region = us-east-1
output = json
" >$AWS_PATH/config

# aws --endpoint https://s3.filebase.com s3 ls
